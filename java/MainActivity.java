package com.example.evotec.practica3_pmbjcp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActivityRegistrar.class);
                startActivity(intent);
                break;


            case R.id.opcionAcelerometro:
                intent = new Intent(MainActivity.this, ActivityAcelerometro.class);
                startActivity(intent);
                break;
            case R.id.opcionLuz:
                intent = new Intent(MainActivity.this, ActivityLuz.class);
                startActivity(intent);
                break;


            case R.id.opcionCamara:
                intent = new Intent(MainActivity.this, ActivityCamara.class);
                startActivity(intent);
                break;
            case R.id.opcionLinterna:
                intent = new Intent(MainActivity.this, ActivityLinterna.class);
                startActivity(intent);
                break;
            case R.id.opcionVibracion:
                intent = new Intent(MainActivity.this, ActivityVibracion.class);
                startActivity(intent);
                break;
        }
        return true;
    }

}
